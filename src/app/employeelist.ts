import { Employee } from './employee';

export const EMPLOYEES: Employee[] = [
  { id: 1, name: 'Priyank' },
  { id: 2, name: 'Dr. Patel' },
  { id: 3, name: 'Krushik' },
  { id: 4, name: 'Karan' },
  { id: 5, name: 'Neha' },
  { id: 6, name: 'Vruti' },
  { id: 7, name: 'Ajay' },
  { id: 8, name: 'Yash' },
  { id: 9, name: 'Megha' },
  { id: 10, name: 'Savan' }
];
