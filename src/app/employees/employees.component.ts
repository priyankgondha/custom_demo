
import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import { EMPLOYEES } from '../employeelist';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employeess = EMPLOYEES;
  selectedemployee!:Employee;
  constructor() { }

  
  ngOnInit(): void {
  }
  onSelect(employee: Employee): void {
    this.selectedemployee = employee;
  }

}
